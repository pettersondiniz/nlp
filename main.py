from Preprocessing.preprocessing import *

if __name__ == "__main__":

    # data - text to process
    #data = Data("Text/quick and easy.txt")
    #text = #data.loadData()
    text = """
<title>Quick and Easy</title>

<body>In Brazil, a lot of people 😍 try to save time. In most families, both the husband and the wife work full-time, 😍 so their free time is very short and very important to them. They look for quick and convenient ways to do their 😍 shopping and other things, so they can have free time to enjoy themselves and do the things they really like to do. 😍 </body>

https://ingleswinner.com/blog/texto-em-ingles-para-expandir-vocabulario/

Back in the day people used to go 😍 to many different places to buy the things they needed at home. They used to go to the butcher for meat and chicken, to the fruit 😍 market for fresh fruit, and to the bakery for bread. Nowadays, most people just go to one place to get everything they need. They can buy fruit, vegetables, bread, meat, milk and frozen foods at their local supermarket. 😍

https://ingleswinner.com/blog/texto-em-ingles-para-expandir-vocabulario/

When people eat out, they often 😍 go to fast food restaurants. There people order their food at the counter and have to wait only a few seconds or minutes. Then they carry their food to a table, sit down, eat it, and throw their trash away. Sometimes people leave their trash on the table for the employees to clean up. It doesn’t take too long to 😍 eat a meal at a fast food restaurant, but it’s not always a healthy choice. 😍

https://ingleswinner.com/blog/texto-em-ingles-para-expandir-vocabulario/
    
    """
    print(text)
    
    #Cleaning
    t = Tokenization()
    c = Cleaning()
    text = c.lower_case(text)
    text = c.remove_emojis(text)
    text = c.remove_urls(text)
    text = c.remove_html_tags(text)
    text = c.remove_ponctuation(text)
    tokens = t.generate(text, 'word')
    tokens = c.remove_stop_words(tokens)
    tokens = c.remove_frequent_words(tokens, 2)
    tokens = c.remove_rare_words(tokens, 2)

    print(tokens)
    l = Lematization()
    print(l.lemmatize(tokens))
    s = Steming()
    print(s.porter(tokens))
    print(s.lancaster(tokens))
    
    
    print(c.spelling_correction(tokens))

    #Tokenization
    #a)
    # t = Tokenization()
    # tokens = t.generate(text, 'word')
    # print(tokens)


    #3) Normalization
    text = "27/03/1995 asdfasdf sdf5sdf asd78fsadf 1/1/70 dfsdg fghdfgh"
    n = Normalization()
    #a)
    # print(n.date2str(test))
    #b)
    # print(n.number2str(test))
    #c)
    # print(n.percent2str("2$ e 23$ 2 0$"))
    #f)
    # text = '😍'
    # print(n.emojis2words(text))
    
