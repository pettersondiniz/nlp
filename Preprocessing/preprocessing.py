"""This module contains the functions used to preprocessing text.
"""
from googletrans import Translator
from num2words import num2words
import nltk
from nltk.corpus import stopwords
import re
from textblob import TextBlob
from nltk.stem import PorterStemmer
from nltk.stem import LancasterStemmer
from nltk.stem import WordNetLemmatizer 
import emoji


class Data():
    """This class contains the implementations and information about text DATA.
    """
    def __init__(self, path):
        self.path = path


    def loadData(self):
        f = open(self.path, "r")
        return f.read()


class Tokenization():
    """This class contains the implementations of tokenization methods.
    """
    def generate(self, text, modo):
        """This method generates a list of tokenized words.

        Args:
            text (str): The imput text.
            modo (str): the imput modo (word or sentence)

        Returns:
            tokens: A list that contains all tokens.
        """
        if modo == 'word':
            tokens = nltk.word_tokenize(text)
            return tokens
        
        return nltk.sent_tokenize(text)

class Cleaning():
    def lower_case(self, text:str):
        """This method aplies a lowe case to the text.

        Args:
            text (str): The input text.

        Returns:
            str: The lowercased text.
        """
        return str(text).lower()

    def remove_ponctuation(self, text:str):
        """This method aplies ponctuation removal to the text.

        Args:
            text (str): The input text.

        Returns:
            text: The text without ponctuation.
        """
        tokenizer = nltk.RegexpTokenizer(r'\w+')
        tokens = tokenizer.tokenize(str(text))
        out = ' '.join(tokens)
        return out

    def remove_stop_words(self, token:list):
        """This method removes the stop words from token.

        Args:
            tokens (list): A list of word tokens.

        Returns:
            list: A list of words without stop words
        """
        stop_words = set(stopwords.words('english')) 
        filtered_sentence = [w for w in token if not w in stop_words]
        return filtered_sentence

    def remove_frequent_words(self, words:list, n:int):
        """This method removes the n most frequent words from list.

        Args:
            words (list): A list of word tokens.
            n (int): Number of words to be removed.

        Returns:
            list: A list of words without frequent words.
        """
        freq = nltk.FreqDist(words)
        most = freq.most_common(n)
        filtered_sentence = [w for w in words if not w in most]
        return filtered_sentence

    def remove_rare_words(self, words:list, n:int):
        """This method removes the n most rare words from list.

        Args:
            words (list): A list of word tokens.
            n (int): Number of words to be removed.

        Returns:
            list: A list of words without rare words.
        """
        freq = nltk.FreqDist(words)
        rare = list(freq.keys())[-n:]
        filtered_sentence = [w for w in words if not w in rare]
        return filtered_sentence

    def remove_emojis(self, text:str):
        """This method removes the emojis from a text

        Args:
            text (str): The input text.

        Returns:
            str: Text without emojis
        """
        emoji_pattern = re.compile("["
            u"\U0001F600-\U0001F64F"  # emoticons
            u"\U0001F300-\U0001F5FF"  # symbols & pictographs
            u"\U0001F680-\U0001F6FF"  # transport & map symbols
            u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
            u"\U00002702-\U000027B0"
            u"\U000024C2-\U0001F251"
            "]+", flags=re.UNICODE)
        return emoji_pattern.sub(r'', text)

    # def remove_emoticons(self):
    #     pass

    def remove_urls(self, text:str):
        """This method removes the urls from a text

        Args:
            text (str): The input text.

        Returns:
            str: Text without urls
        """
        text = re.sub(r'^https?:\/\/.*[\r\n]*', '', text, flags=re.MULTILINE)
        return text

    def remove_html_tags(self, text:str):
        """This method removes the html tags from a text

        Args:
            text (str): The input text.

        Returns:
            str: Text without tags
        """
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', text)
        return cleantext

    def chat_words(self):
        pass

    def reduce_lengthening(self, text:str):
        """This method reduces characters wrongly repeated

        Args:
            text (str): The input text.

        Returns:
            str: Text without repeats
        """
        pattern = re.compile(r"(.)\1{2,}")
        return pattern.sub(r"\1\1", text)


    def spelling_correction(self, words:list):
        """This method fix the text spelling

        Args:
            words (list): list of words

        Returns:
            list: Words with spell correction
        """
        out = []
        for w in words:
            w_red = self.reduce_lengthening(w)
            w_cor = str(TextBlob(w_red).correct())
            out.append(w_cor)
        return out

class Normalization():
    """This class contains the implementations of normalization methods.
    """
    def __init__(self):        
        self.months = {
            1: 'Janeiro', 2: 'Fevereiro', 3: 'Março', 4: 'Abril', 5: 'Maio', 6: 'Junho',
            7: 'Julho', 8: 'Agosto', 9: 'Setembro', 10: 'Outubro', 11: 'Novembro', 12: 'Dezembro'       
        }

        self.translator = Translator()

    def date2str(self, text):
        """This method convert a date to a string date full.

        Args:
            text (str): The imput text.
            
        Returns:
            datesStr: A string date full.
        """

        dates =  re.findall(r'\d+\S\d+\S\d+', text)
        datesStr = []
        for date in dates:
            date = date.replace("-", "/")
            day, month, year = date.split('/')
            day, year = num2words(int(day), lang='pt_BR'), num2words(int(year), lang='pt_BR')
            month = self.months[int(month)]
            datesStr.append(day +" de "+ month +" de "+ year)  
        
        return datesStr

    def number2str(self, text):
        """This method convert a number to a string number.

        Args:
            text (str): The imput text.
            
        Returns:
            numbersStr: A string number.
        """

        numbers = re.findall(r'\d*', text)
        numbers.remove('')
        numbersStr = []
        for num in numbers:
            numbersStr.append(num2words(int(num), lang='pt_BR'))
        
        return numbersStr 
        
        
    def percent2str(self, text):
        """This method convert simbol "$" to string name.

        Args:
            text (str): The imput text.
            
        Returns:
            text: A text with simbols replace.
        """
        numbers = re.findall(r'\d*', text)
        numbers.remove('')
        for num in numbers:
            if(num == '1'):
                text = text.replace(str(num)+"$", str(num) +" dólar Americano")
            else:         
                text = text.replace(str(num)+"$", str(num) +" dólares Americano")
        
        return text

    def spelling_mistakes_correction(self):
        pass

    def emoticons2words(self):
        pass

    def emojis2words(self, text):
        """This method convert emoji to word.

        Args:
            text (str): The imput text.
            
        Returns:
            trans.text: The translated word of the emojis.
        """
        word = emoji.demojize(text)
        word = word.replace(":","")
        word = word.replace("_"," ")
        trans = self.translator.translate(word, dest='pt_BR')
        return trans.text


class Lematization():
    def lemmatize(self, words:list):
        lemmatizer = WordNetLemmatizer()
        out = []
        for w in words:
            out.append(lemmatizer.lemmatize(w))
        return out

class Steming():
    def porter(self, words:list):
        porter = PorterStemmer()
        out = []
        for w in words:
            out.append(porter.stem(w))
        return out

    def lancaster(self, words:list):
        lancaster = LancasterStemmer()
        out = []
        for w in words:
            out.append(lancaster.stem(w))
        return out